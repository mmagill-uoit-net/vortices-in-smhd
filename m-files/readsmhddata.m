


function [A] = readsmhddata(filename,timestep)

  % timestep must be an integer in [0,92]
  % filename is the .dat file containing the data
  % The names are formatted as:
  %    10_1_65_100_2_1e-6_1e10_1_0_u1.dat
  %
  % where you will only need to change:
  %    1e10, the circulation
  %    u1, the variable you want to look at
  %
  filename
  timestep 
  file = fopen(filename);

  % Skip to beginning of timestep
  for nn = 1:(timestep*1024)
	     fgetl(file);
  end

  % Read timestep into an array
  A = zeros(1024,1024);
  for ii = 1:1024
     linestring = fgetl(file);
     linearray = sscanf(linestring,'%f');
     A(ii,:) = linearray';
  end

  
  
