% This script compares two different latitudes
% Figure 1 shows the KE
% Figure 10 shows the magentic energy
% betterplots is called to make it look nice

lats=(1:1024)/1024;
lons=(1:1024)/1024;
cntr=0
figure(1)
clf
colormap hot
betterplots
figure(10)
clf
betterplots
colormap hot
for timestep=5:5:15
cntr=cntr+1;

timestep=timestep*5;
G='1e2';
Bfac='1';


casename=sprintf('../data/10_1_65_100_2_1e-6_%s_%s_0',G,Bfac);

myvort=readsmhddata(sprintf('%s_vort_%05d.dat',casename,timestep),0);
myeta=readsmhddata(sprintf('%s_e_%05d.dat',casename,timestep),0);
myu1=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep),0);
myu2=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep),0);
myb1=readsmhddata(sprintf('%s_b1_%05d.dat',casename,timestep),0);
myb2=readsmhddata(sprintf('%s_b2_%05d.dat',casename,timestep),0);

myens=0.5*myvort.^2;
myape=0.5*myeta.^2;
myke=0.5*(myu1.^2+myu2.^2);
mymag=0.5*((myb1-myb1(1,1)).^2+myb2.^2);
if cntr==1
mxens=max(myens(:));
mxape=max(myape(:));
mxke=max(myke(:));
mxmag=max(mymag(:));
end
figure(1)
subplot(2,3,cntr)
contourf(lats,lons,myke,20),shading flat%colorbar
caxis([0 0.75]*mxke)
axis square
figure(10)
subplot(2,3,cntr)
contourf(lats,lons,mymag,20),shading flat%colorbar
caxis([0 0.75]*mxmag)
%caxis([-1 1]*1*10)
axis square
drawnow

timestep=timestep;
G='1e2';
Bfac='1';


casename=sprintf('../data/10_1_0_100_2_1e-6_%s_%s_0',G,Bfac);

myvort=readsmhddata(sprintf('%s_vort_%05d.dat',casename,timestep),0);
myeta=readsmhddata(sprintf('%s_e_%05d.dat',casename,timestep),0);
myu1=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep),0);
myu2=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep),0);
myb1=readsmhddata(sprintf('%s_b1_%05d.dat',casename,timestep),0);
myb2=readsmhddata(sprintf('%s_b2_%05d.dat',casename,timestep),0);

myens=0.5*myvort.^2;
myape=0.5*myeta.^2;
myke=0.5*(myu1.^2+myu2.^2);
mymag=0.5*((myb1-myb1(1,1)).^2+myb2.^2);
if cntr==1
mxens2=max(myens(:));
mxape2=max(myape(:));
mxke2=max(myke(:));
mxmag2=max(mymag(:));
end
figure(1)
subplot(2,3,cntr+3)
contourf(lats,lons,myke,20),shading flat%colorbar
caxis([0 0.75]*mxke2)
axis square
figure(10)
subplot(2,3,cntr+3)
contourf(lats,lons,mymag,20),shading flat%colorbar
caxis([0 0.75]*mxmag2)
axis square
drawnow
end
figure(1)
subplot(2,3,1)
ylabel('lat')
subplot(2,3,4)
ylabel('lat')
for ni=4:6
subplot(2,3,ni)
xlabel('lon')
end
figure(10)
subplot(2,3,1)
ylabel('lat')
subplot(2,3,4)
ylabel('lat')
for ni=4:6
subplot(2,3,ni)
xlabel('lon')
end
