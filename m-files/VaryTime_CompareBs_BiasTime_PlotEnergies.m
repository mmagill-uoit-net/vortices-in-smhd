
for timestep=30:30:90

    G='1e2';
    Bfac='0.5';
    analyse_energies(timestep,G,Bfac);



    timestep=timestep/2;
    G='1e2';
    Bfac='1';
    analyse_energies(timestep,G,Bfac);
   

end
