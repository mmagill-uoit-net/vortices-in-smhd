
timestep=120;
G='1e2';
Bfac='0.5';
analyse_energies(timestep,G,Bfac);


timestep=60;
G='1e2';
Bfac='1';
analyse_energies(timestep,G,Bfac);


timestep=60;
G='5e2';
Bfac='1';
analyse_energies(timestep,G,Bfac);

