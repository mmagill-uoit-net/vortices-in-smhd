#!/bin/bash


# Run the production cases, designed to test the same vortex ICs in various dimensional regimes.
# Always same: resolution 1024^2, aspect ratio 1, overshoot layer, magnetic field on, no plotting
# Case parameters: latitude, width, buoyancy
LogRes=10
AspRatio=1
Layer=2
MagFlag=1
PlotFlag=0

latitude=65
width=100
alpha=1e-6


#                            log2(Ny)  (Nx/Ny)  latitude  Lx(degs)  Rad,Ovr  alpha  G   mag_flag   plot_flag   field/axis
G=5e1
#time ./bin/spectralsmhd.exe  $LogRes $AspRatio $latitude  $width    $Layer  $alpha  $G  $MagFlag   $PlotFlag
G=1e2
#time ./bin/spectralsmhd.exe  $LogRes $AspRatio $latitude  $width    $Layer  $alpha  $G  $MagFlag   $PlotFlag
G=5e2
#time ./bin/spectralsmhd.exe  $LogRes $AspRatio $latitude  $width    $Layer  $alpha  $G  $MagFlag   $PlotFlag


# Run low-B cases
G=1e2
MagFlag=0
#time ./bin/spectralsmhd.exe  $LogRes $AspRatio $latitude  $width    $Layer  $alpha  $G  $MagFlag   $PlotFlag
MagFlag=0.5
#time ./bin/spectralsmhd.exe  $LogRes $AspRatio $latitude  $width    $Layer  $alpha  $G  $MagFlag   $PlotFlag
MagFlag=1


# Run Super Vortex at high res
G=5e2
LogRes=12
#time ./bin/spectralsmhd.exe  $LogRes $AspRatio $latitude  $width    $Layer  $alpha  $G  $MagFlag   $PlotFlag
LogRes=10
G=5e1


# Run Super Vortex at equator with beta plane
G=5e2
latitude=0
LogRes=10
time ./bin/spectralsmhd.exe  $LogRes $AspRatio $latitude  $width    $Layer  $alpha  $G  $MagFlag   $PlotFlag

