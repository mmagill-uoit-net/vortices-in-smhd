

#include "variables.h"


void InitializeVariable(struct Variables* Variable, int Nx, int Ny) {

   Variable->Nx = Nx;
   Variable->Ny = Ny;

   Variable->g = malloc(Nx*Ny * sizeof(double));
   Variable->gx = malloc(Nx*Ny * sizeof(double));
   Variable->gy = malloc(Nx*Ny * sizeof(double));

   // FFTW wants first dimension to be number of rows, second to be number of colums.
   // I want Nx to be number of bins parallel to equator
   // Thus Ny goes first, Nx goes second.
   Variable->G = malloc(Ny*(Nx/2+1) * sizeof(fftw_complex));
   Variable->GX = malloc(Ny*(Nx/2+1) * sizeof(fftw_complex));
   Variable->GY = malloc(Ny*(Nx/2+1) * sizeof(fftw_complex));
   Variable->fft_g = fftw_plan_dft_r2c_2d(Ny, Nx, Variable->g, Variable->G, 0);
   Variable->ifft_GX = fftw_plan_dft_c2r_2d(Ny, Nx, Variable->GX, Variable->gx, 0);
   Variable->ifft_GY = fftw_plan_dft_c2r_2d(Ny, Nx, Variable->GY, Variable->gy, 0);


}


void FreeVariable(struct Variables* Variable) {

   fftw_destroy_plan(Variable->fft_g);
   fftw_destroy_plan(Variable->ifft_GX);
   fftw_destroy_plan(Variable->ifft_GY);

   free(Variable->g);
   free(Variable->gx);
   free(Variable->gy);

   free(Variable->G);
   free(Variable->GX);
   free(Variable->GY);

}




void UpdateDerivs(struct Variables* Variable, double* k, double* l, double* k_filter, double* l_filter) {

   // Compute G
   fftw_execute(Variable->fft_g);

   // Compute GX and GY from G
   int kk;
   for (int jj=0; jj<Variable->Ny; jj++) {
   for (int ii=0; ii<Variable->Nx/2 + 1; ii++) {
      kk = ii + jj*(Variable->Nx/2+1);

      Variable->GX[kk][0] = -k[ii] * Variable->G[kk][1] * k_filter[ii]
                                                          /(Variable->Nx*Variable->Ny);
      Variable->GX[kk][1] =  k[ii] * Variable->G[kk][0] * k_filter[ii]
                                                          /(Variable->Nx*Variable->Ny);

      Variable->GY[kk][0] = -l[jj] * Variable->G[kk][1] * l_filter[jj]
                                                          /(Variable->Nx*Variable->Ny);
      Variable->GY[kk][1] =  l[jj] * Variable->G[kk][0] * l_filter[jj]
                                                          /(Variable->Nx*Variable->Ny);

      
   }}

   // Compute gx and gy from their FFTs
   // These overwrite GX and GY!
   fftw_execute(Variable->ifft_GX);
   fftw_execute(Variable->ifft_GY);

}

