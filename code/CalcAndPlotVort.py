from glob import glob
import os
import numpy as np
import matplotlib.pyplot as plt
my_dpi=96


blocksize = 4096

fig = plt.figure()
fig.set_size_inches(8,8)
ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
fig.add_axes(ax)

fnames_u1 = glob('data/12*_u1.dat')
fnames_u2 = glob('data/12*_u2.dat')
artist = ax.imshow(np.ndarray((blocksize,blocksize),dtype=float), vmin=-1, vmax=1, aspect='auto')

Uone = np.ndarray((blocksize,blocksize),dtype=float)
Utwo = np.ndarray((blocksize,blocksize),dtype=float)
FFT_Uone = np.ndarray((blocksize,blocksize),dtype=complex)
FFT_Utwo = np.ndarray((blocksize,blocksize),dtype=complex)
Vort = np.ndarray((blocksize,blocksize),dtype=float)

for i in range(len(fnames_u1)):

    print("Plotting %s" % fnames[i])

    with open(fnames_u1[i],'r') as fu1, open(fnames_u2[i],'r') as fu2:

	basename = os.path.basename(fnames_u1[i])
	for j, line_u1 in enumerate(fu1), line_u2 in enumerate(fu2):

		k = j % blocksize

		Uone[k,:] = np.array(line_u1.strip().split("\t"))
		Utwo[k,:] = np.array(line_u2.strip().split("\t"))

                # When Uone and Utwo are filled for a case
		if (j % blocksize) == (blocksize-1):
			plotnum = int(j/blocksize)+1
			print("\t Plot %d" % plotnum)

                        # Compute vorticity
                        FFT_Uone = np.fft.fft2(Uone)
                        FFT_Utwo = np.fft.fft2(Utwo)
                        Vort =

                        # Plot and save
			artist.set_data(Vort)
			plt.draw()
			plt.savefig("plots/%s_plot%d.png" % (basename,plotnum), dpi=5*my_dpi, bbox_inches=0)
