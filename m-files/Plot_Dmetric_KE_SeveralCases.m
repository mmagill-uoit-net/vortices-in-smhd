

% Run in floor-level folder, so add code folder
addpath('new-m-files');

% Time
tmax=30;
t = 0:tmax;

% Reference
DmetricKE_Reference = Compute_Dmetric_KE('65','1e2','1',tmax);

% Scope G
DmetricKE_HalvedG = Compute_Dmetric_KE('65','5e1','1',tmax);
DmetricKE_DoubledG = Compute_Dmetric_KE('65','5e2','1',tmax);
DmetricKE_antiHalvedG = Compute_Dmetric_KE('65','-5e1','1',tmax);
DmetricKE_antiReference = Compute_Dmetric_KE('65','-1e2','1',tmax);
DmetricKE_antiDoubledG = Compute_Dmetric_KE('65','-5e2','1',tmax);

% Scope B
DmetricKE_B00 = Compute_Dmetric_KE('65','1e2','0',tmax);
DmetricKE_B01 = Compute_Dmetric_KE('65','1e2','0.1',tmax);
DmetricKE_B05 = Compute_Dmetric_KE('65','1e2','0.5',tmax);

% Scope lat
DmetricKE_L00 = Compute_Dmetric_KE('0','1e2','1',tmax);

% Save
save('Dmetrics_KE.mat');


 % Plots
 subplot(3,1,1)
   plot(t,DmetricKE_Reference,t,DmetricKE_HalvedG,t,DmetricKE_DoubledG,t,DmetricKE_antiReference,t,DmetricKE_antiHalvedG,t,DmetricKE_antiDoubledG);
 title('Scope G')
 xlabel('Time')
 ylabel('Dmetric According to KE')
 legend('Reference','Halved G','Doubled G','Anti Reference','Anti Halved G','Anti Doubled G')
 
 subplot(3,1,2)
 plot(t,DmetricKE_Reference,t,DmetricKE_B05,t,DmetricKE_B01,t,DmetricKE_B00);
 title('Scope G')
 xlabel('Time')
 ylabel('Dmetric According to KE')
 legend('Reference','B = 0.5 B0','B = 0.1 B0','B = 0.0 B0')

 subplot(3,1,3)
 plot(t,DmetricKE_Reference,t,DmetricKE_L00)
 title('Scope Latitude')
 xlabel('Time')
 ylabel('Dmetric According to Eta')
 legend('Reference','Lat = 0')
