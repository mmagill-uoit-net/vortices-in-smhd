

function plot_energies(myens,myke,myape,mymag)

    clf
    colormap hot
    subplot(2,2,1)
    imagesc(myens),shading interp,colorbar
    title('Enstropy')
    subplot(2,2,2)
    imagesc(myke),shading interp,colorbar
    title('Kinetic Energy')
    subplot(2,2,3)
    imagesc(myape),shading interp,colorbar
    title('Available Potential Energy')
    subplot(2,2,4)
    imagesc(mymag),shading interp,colorbar
    title('Magnetic Field Energy')
    drawnow
    