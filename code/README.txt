--- Compiling

Doing 'make' in this directory should compile the source code from 'src', store the object files in 'obj', and publish the executable in 'bin'. This will compile with OpenMP. To compile without, do 'make OPENMP='. 

NOTE: When recompiling with OpenMP from without OpenMP or vice versa, do 'make clean' before 'make'


--- Output

Data is automatically saved in the 'data' folder. The files are named according to:

	[The list of command line arguments used to generate the data, separated by underscores]_[variable name].dat

[variable name] can be

	e:	Height distribution
	u1:	x velocity
	u2:	y velocity
	b1:	x magnetic field
	b2:	y magnetic field


Running 'gnuplot < plot_heights.p' will read data from 'data' and plot it, saving the plots in 'plots'. It doesn't work perfectly currently, and 
the file will need manual tweaking before running it each time. Post-processing the data into plots isn't a priority right now.


--- Testing Suite

The testing suite is run via timing_tests.sh. It stores the results in times.dat, created locally. The 'tests' folder contains previous tests.

The script 'plot_times.sh' post-processes the output of the testing suite, then compiles that into gnuplot plots.


--- Misc

The file 'parallelization_notes.txt' chronicles the conversion from the serial code to the parallel code.


--- Log taken from Vortex Work, which was re-incorporated on June 30th 2015

Copied from main source May 1, 10 am

Changes made from source not related to the vorticity work:

	Renormalizing write-to-file output in the same way as plotting output.

	Improve plot_data.sh (still lots of room for improvement)

	Improve plot_data.sh significantly more.

	Fix gnuplot bug in plot_data.sh: need to specify png terminal settings or duplicate plots.

	Still unreliable. Switched to eps (from png). Added 'convert' to plot_data.sh.

	Fixed time settings to only go until mag waves reach BCs
	Fixed time settings to make at least 90 saves

	Applied exponential envelope to the vortex due to BC shocks

	Added initial circulation G as a command line input
