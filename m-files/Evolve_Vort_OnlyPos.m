G='5e2';
Bfac='1';
casename=sprintf('../code/data/10_1_0_10_2_1e-6_%s_%s_0',G,Bfac);
labels='abcdefghi';

figure(22)
clf
colormap darkjet
for ii=1:9
    timestep=5*ii

    mystr=sprintf('%s_u1_%05d.dat',casename,timestep);
    myu1=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep),0);
    myu2=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep),0);

    % Wave vectors
    N=1024;
    L=(100./360.) * 2*pi*0.7*6.955e8;
    k=[0:N/2-1 0 -N/2+1:-1]';
    dk = pi / L; k = dk*k; l=k;
    [kk,ll]=meshgrid(k,l);
    
    % Vorticity
    myu1y = real(ifft2( i* ll.*fft2(myu1) ));
    myu2x = real(ifft2( i* kk.*fft2(myu2) ));
    myvort = myu2x - myu1y;

    subplot(3,3,ii)
    imagesc(myvort/max(myvort(:))),shading interp
    caxis([-1 1])
    axis equal
    axis off
    strnow=['(' labels(ii) ')'];
    text(100,100,strnow,'Color','w')
end
tightfig
