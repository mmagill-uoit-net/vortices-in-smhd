import numpy as np
import matplotlib.pyplot as plt
import matplotlib

fontsize = 22
matplotlib.rcParams['lines.linewidth']=3
### Dimensional regime analyser for the solar tachocline

## Constants

# Universal
mu0 = (4*np.pi)*1e-7

# Radii
r_sun = 6.955e8
r_tach = 0.7 * r_sun
C_tach = 2*np.pi * r_tach

# Layer thicknesses
h_tach_rad = 1.e7
h_tach_ovr = 5.e6

# Rotation
def omega_sun_in_degrees(lat_in_radians):
    omegaA = 14.713
    omegaB = -2.396
    omegaC = -1.787
    omega = (omegaA +
             omegaB*np.sin(lat_in_radians)**2 +
             omegaC*np.sin(lat_in_radians)**4)
    return omega

# Gravity
g_sun = 274.
g_tach = g_sun / (r_tach/r_sun)**2
gR_tach_rad = 1e-1 * g_tach
gR_tach_ovr = 1e-6 * g_tach

# Magnetic field
B_tach = 10.

# Density
rho_tach = 210.


## Calculate dimensionless parameters for given scales

def CalcParams():
    Fr = CalcFr(U,H,gR)
    Ro = CalcRo(U,L,f)
    Co = CalcCo(U,rho,B)
    return Fr,Ro,Co

def CalcFr(U,H,gR):
    Fr = U / np.sqrt(H*gR)
    return Fr

def CalcRo(U,L,f):
    Ro = U / (L*f)
    return Ro

def CalcCo(U,rho,B):
    Co = B**2 / (U**2 * mu0*rho)
    return Co


## Plot
## U = gravity wave speed or alfven wave speed
## B = 10 T
## rho = 210 kg/m3
## L and f vary 
## L is given in degrees of longitude
## f is a function of latitude

# Range of f: tach rotates with latitude 45
def SunLat_into_Coriolis(lat_in_radians):
    # Omega in deg/day
    omega_in_degrees = omega_sun_in_degrees(45 * np.pi/180.)
    # Omega in rad/day
    omega_in_radians = omega_in_degrees * np.pi/180.
    # Omega in rad/s
    omega_in_radpsec = omega_in_radians / (60*60*24.)
    f_in_radians = 2*omega_in_radpsec*np.sin(lat_in_radians)
    return f_in_radians
lats = np.array([5,30,65])
lats = lats*np.pi/180.
f = SunLat_into_Coriolis(lats)

# Range of L
def DegreesOfLong_into_Length(deg_of_long,C):
    return C * deg_of_long/360.
L_in_deg_of_long = np.linspace(0.01,100,1000)

## Plot the gravity wave case
fig1, axarr1 = plt.subplots(3,2,sharex=True)
c0_tach_rad = np.sqrt(h_tach_rad*gR_tach_rad)
c0_tach_ovr = np.sqrt(h_tach_ovr*gR_tach_ovr)
for i in range(len(f)):

    L = DegreesOfLong_into_Length(L_in_deg_of_long,C_tach*np.cos(lats[i]))

    ## Plot radiative layer case
    Frs = CalcFr(c0_tach_rad,h_tach_rad,gR_tach_rad)
    Ros = CalcRo(c0_tach_rad,L,f[i])
    Cos = CalcCo(c0_tach_rad,rho_tach,B_tach)
    axarr1[i,0].axhline(np.abs(1./Frs**2),color='c')
    axarr1[i,0].plot(L_in_deg_of_long,np.abs(1./Ros)   ,color='g')
    axarr1[i,0].axhline(np.abs(Cos)      ,color='m')
    axarr1[i,0].set_title("Latitude = %d degrees" % round(lats[i]*180./np.pi))
    axarr1[i,0].set_ylabel("Radiative Layer")

    ## Plot overshoot layer case
    Frs = CalcFr(c0_tach_ovr,h_tach_ovr,gR_tach_ovr)
    Ros = CalcRo(c0_tach_ovr,L,f[i])
    Cos = CalcCo(c0_tach_ovr,rho_tach,B_tach)
    axarr1[i,1].axhline(np.abs(1./Frs**2),color='c')
    axarr1[i,1].plot(L_in_deg_of_long,np.abs(1./Ros)   ,color='g')
    axarr1[i,1].axhline(np.abs(Cos)      ,color='m')
    axarr1[i,1].set_title("Latitude = %d degrees" % round(lats[i]*180./np.pi))
    axarr1[i,1].set_ylabel("Overshoot Layer")

for i in range(3):
    for j in range(2):
        axarr1[i,j].set_xlim([0.01,100])
        axarr1[i,j].set_ylim([1e-5,1e3])
        axarr1[i,j].set_xscale('log')
        axarr1[i,j].set_yscale('log')
        axarr1[i,j].set_yticks([1e-4,1e-2,1e0,1e2])

axarr1[2,0].set_xlabel('L, degrees of longitude')
axarr1[2,1].set_xlabel('L, degrees of longitude')

fig1.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.90, wspace=0.25, hspace=0.20)

axarr1[0,0].text(1e1,1e-2,'1/Ro',color='g',fontsize=fontsize)
axarr1[0,0].text(1e1,5e0,'1/Fr^2',color='c',fontsize=fontsize)
axarr1[0,0].text(1e1,5e-5,'Co',color='m',fontsize=fontsize)

fig1.suptitle("Dimensional Regimes for Gravity Waves")
matplotlib.rcParams.update({'font.size': fontsize})
#fig1.show()



## Plot the alfven wave case
fig2, axarr2 = plt.subplots(3,2,sharex=True)
cA_tach_rad = B_tach / np.sqrt(mu0*rho_tach)
cA_tach_ovr = B_tach / np.sqrt(mu0*rho_tach)
for i in range(len(f)):

    L = DegreesOfLong_into_Length(L_in_deg_of_long,C_tach*np.cos(lats[i]))

    ## Plot radiative layer case
    Frs = CalcFr(cA_tach_rad,h_tach_rad,gR_tach_rad)
    Ros = CalcRo(cA_tach_rad,L,f[i])
    Cos = CalcCo(cA_tach_rad,rho_tach,B_tach)
    axarr2[i,0].axhline(np.abs(1./Frs**2),color='c')
    axarr2[i,0].plot(L_in_deg_of_long,np.abs(1./Ros)   ,color='g')
    axarr2[i,0].axhline(np.abs(Cos)      ,color='m')
    axarr2[i,0].set_title("Latitude = %d degrees" % round(lats[i]*180./np.pi))
    axarr2[i,0].set_ylabel("Radiative Layer")

    ## Plot overshoot layer case
    Frs = CalcFr(cA_tach_ovr,h_tach_ovr,gR_tach_ovr)
    Ros = CalcRo(cA_tach_ovr,L,f[i])
    Cos = CalcCo(cA_tach_ovr,rho_tach,B_tach)
    axarr2[i,1].axhline(np.abs(1./Frs**2),color='c')
    axarr2[i,1].plot(L_in_deg_of_long,np.abs(1./Ros)   ,color='g')
    axarr2[i,1].axhline(np.abs(Cos)      ,color='m')
    axarr2[i,1].set_title("Latitude = %d degrees" % round(lats[i]*180./np.pi))
    axarr2[i,1].set_ylabel("Overshoot Layer")


for i in range(3):
    for j in range(2):
        axarr2[i,j].set_xlim([0.01,100])
        axarr2[i,j].set_ylim([1e-4,1e4])
        axarr2[i,j].set_xscale('log')
        axarr2[i,j].set_yscale('log')
        axarr2[i,j].set_yticks([1e-3,1e-1,1e1,1e3])

axarr2[2,0].set_xlabel('L, degrees of longitude')
axarr2[2,1].set_xlabel('L, degrees of longitude')

fig2.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.90, wspace=0.25, hspace=0.20)

axarr2[0,0].text(2e0,1e-3,'1/Ro',color='g',fontsize=fontsize)
axarr2[0,0].text(2e0,1e2,'1/Fr^2',color='c',fontsize=fontsize)
axarr2[0,0].text(2e0,1e-1,'Co',color='m',fontsize=fontsize)

fig2.suptitle("Dimensional Regimes for Alfven Waves")
matplotlib.rcParams.update({'font.size': fontsize})
fig2.show()
