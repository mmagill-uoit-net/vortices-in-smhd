

function [Dmetric] = Compute_Dmetric_KE(lat,G,Bfac,maxT)

Dmetric = zeros(maxT+1,1);

for timestep=0:maxT
    
    % Load
    casename=sprintf('./data/10_1_%s_100_2_1e-6_%s_%s_0',lat,G,Bfac);
    [myvort, myeta, myu1, myu2, myb1, myb2] = full_read(casename,timestep);
    
    % Compute fields
    [myens, myape, myke, mymag] = compute_energies(myvort,myeta,myu1,myu2,myb1,myb2);
    
    % Spatial grid (might need to rethink this - just using degrees lat/lon for now)
    x = linspace(-100,100,2^10);
    y = linspace(-100,100,2^10);
    [XX,YY] = meshgrid(x,y);
    
    % Compute Where the Wave has crossed threshold in eta
    mxke = max(myke(:));
    threshold_in_ke = 0.5*mxke;
    xWaveExtent_AccordingToKE = XX(myke>threshold_in_ke);
    yWaveExtent_AccordingToKE = YY(myke>threshold_in_ke);
    dist_east = max(xWaveExtent_AccordingToKE(:));
    dist_north = max(yWaveExtent_AccordingToKE(:));
    Dmetric_KE = dist_east / dist_north;
    
    % If empty, metric is 0 by default
    if isempty(Dmetric_KE)
        Dmetric_KE = 0;
    end
    
    %     % Plot to check if metric accurately captures aspect ratio
    %     figure('Name',sprintf('Timestep %d, G = %s, B = %s B_0',timestep,G,Bfac),'NumberTitle','off')
    %     plot_energies(myens,myke,myape,mymag);
    
    Dmetric(timestep+1) = Dmetric_KE;
    
end
