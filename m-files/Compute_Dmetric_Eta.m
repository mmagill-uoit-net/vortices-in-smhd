

function [Dmetric] = Compute_Dmetric_Eta(lat,G,Bfac,maxT)

Dmetric = zeros(maxT+1,1);

for timestep=0:maxT
    
    % Load
	       casename=sprintf('./data/10_1_%s_100_2_1e-6_%s_%s_0',lat,G,Bfac);
    [myvort, myeta, myu1, myu2, myb1, myb2] = full_read(casename,timestep);
    
    % Compute fields
    [myens, myape, myke, mymag] = compute_energies(myvort,myeta,myu1,myu2,myb1,myb2);
    
    % Spatial grid (might need to rethink this - just using degrees lat/lon for now)
    x = linspace(-100,100,2^10);
    y = linspace(-100,100,2^10);
    [XX,YY] = meshgrid(x,y);
    
    % Compute Where the Wave has crossed threshold in eta
    mxeta = max(myeta(:));
    threshold_in_eta = 0.5*mxeta;
    xWaveExtent_AccordingToEta = XX(myeta>threshold_in_eta);
    yWaveExtent_AccordingToEta = YY(myeta>threshold_in_eta);
    dist_east = max(xWaveExtent_AccordingToEta(:));
    dist_north = max(yWaveExtent_AccordingToEta(:));
    Dmetric_eta = dist_east / dist_north;
    
    % If empty, metric is 0 by default
    if isempty(Dmetric_eta)
        Dmetric_eta = 0;
    end
    
    %     % Plot to check if metric accurately captures aspect ratio
    %     figure('Name',sprintf('Timestep %d, G = %s, B = %s B_0',timestep,G,Bfac),'NumberTitle','off')
    %     plot_energies(myens,myke,myape,mymag);
    
    Dmetric(timestep+1) = Dmetric_eta;
    
end
