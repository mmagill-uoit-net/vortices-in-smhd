from glob import glob
import os
import numpy as np
import matplotlib.pyplot as plt
my_dpi=96


fig = plt.figure()
fig.set_size_inches(8,8)
ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
fig.add_axes(ax)

fnames = glob('data/8*_vort_*.dat')

for file in fnames:

    print file
    A = np.loadtxt(file)

    plt.pcolor(A)
    plt.show()



# for i in range(len(fnames)):

#     print("Plotting %s" % fnames[i])

#     with open(fnames[i],'r') as fin:

# 	basename = os.path.basename(fnames[i])
# 	for j, line in enumerate(fin):

# 		k = j % blocksize

# 		A[k,:] = np.array(line.strip().split("\t"))
# 		A = A / np.max(A)

# 		if (j % blocksize) == (blocksize-1):
# 			plotnum = int(j/blocksize)+1
# 			print("\t Plot %d" % plotnum)
# 			artist.set_data(A)
# 			plt.draw()
# 			plt.savefig("plots/%s_plot%d.png" % (basename,plotnum), dpi=5*my_dpi, bbox_inches=0)
