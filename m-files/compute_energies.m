

function [myens, myape, myke, mymag] = compute_energies(myvort,myeta,myu1,myu2,myb1,myb2)

    myens=0.5*myvort.^2;
    myape=0.5*myeta.^2;
    myke=0.5*(myu1.^2+myu2.^2);
    mymag=0.5*((myb1-myb1(1,1)).^2+myb2.^2);
