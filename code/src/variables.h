#ifndef _VARIABLES_H_
#define _VARIABLES_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>



struct Variables {

   // Store the sizes
   int Nx;
   int Ny;

   // Store the variable, its transforms, its derivatives, and their transforms
   double* g;
   fftw_complex* G;
   double* gx;
   double* gy;
   fftw_complex* GX;
   fftw_complex* GY;

   // Store all plans
   // Need to go:
   //   - From g to G
   //   - Compute GX and GY from G (external to fftw)
   //   - From GX to gx an GY to gy
   fftw_plan fft_g;          // g into G
   fftw_plan ifft_GX;        // GX into gx
   fftw_plan ifft_GY;        // GY into gy

};


//// //// //// //// //// ////

void InitializeVariable(struct Variables*,int,int);
void FreeVariable(struct Variables*);

//// //// //// //// //// ////

void UpdateDerivs(struct Variables*, double*, double*, double*, double*);  // Compute gx and gy from g given wavevectors and filters in Fourier space

//// //// //// //// //// ////


#endif
