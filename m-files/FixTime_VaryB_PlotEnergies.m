
%% Timestep 30, no-B case

% Load 
ii=30;
G='1e2';
Bfac='0';
analyse_energies(ii,G,Bfac);


%% Timestep 30, 10% B case

% Load
ii=30;
G='1e2';
Bfac='0.1';
analyse_energies(ii,G,Bfac);


%% Timestep 30, 50% B case

% Load
ii=30;
G='1e2';
Bfac='0.5';
analyse_energies(ii,G,Bfac);


%% Timestep 30, Reference Case
ii=30;
G='1e2';
Bfac='1';
analyse_energies(ii,G,Bfac);
