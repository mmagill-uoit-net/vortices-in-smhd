lats=(1:1024)/1024;
lons=(1:1024)/1024;
cntr=0
figure(1)
clf
colormap darkjet
betterplots
figure(10)
clf
betterplots
colormap darkjet
for timestep=20:20:60
cntr=cntr+1;

G='1e2';
Bfac='1';


casename=sprintf('./data/10_1_65_100_2_1e-6_%s_%s_0',G,Bfac);

myvort=readsmhddata(sprintf('%s_vort_%05d.dat',casename,timestep),0);
myeta=readsmhddata(sprintf('%s_e_%05d.dat',casename,timestep),0);
myu1=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep),0);
myu2=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep),0);
myb1=readsmhddata(sprintf('%s_b1_%05d.dat',casename,timestep),0);
myb2=readsmhddata(sprintf('%s_b2_%05d.dat',casename,timestep),0);

myens=0.5*myvort.^2;
myape=0.5*myeta.^2;
myke=0.5*(myu1.^2+myu2.^2);
mymag=0.5*((myb1-myb1(1,1)).^2+myb2.^2);
if cntr==1
mxens=max(myens(:));
mxape=max(myape(:));
mxke=max(myke(:));
mxmag=max(mymag(:));
end
figure(1)
subplot(2,3,cntr)
contourf(lats,lons,myens,80),shading flat%colorbar
caxis([0 1]*mxens)
axis square
figure(10)
subplot(2,3,cntr)
contourf(lats,lons,mymag,80),shading flat%colorbar
caxis([0 1]*mxmag)
%caxis([-1 1]*1*10)
axis square
drawnow

G='5e2';
Bfac='1';


casename=sprintf('./data/10_1_65_100_2_1e-6_%s_%s_0',G,Bfac);

myvort=readsmhddata(sprintf('%s_vort_%05d.dat',casename,timestep),0);
myeta=readsmhddata(sprintf('%s_e_%05d.dat',casename,timestep),0);
myu1=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep),0);
myu2=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep),0);
myb1=readsmhddata(sprintf('%s_b1_%05d.dat',casename,timestep),0);
myb2=readsmhddata(sprintf('%s_b2_%05d.dat',casename,timestep),0);

myens2=0.5*myvort.^2;
myape2=0.5*myeta.^2;
myke2=0.5*(myu1.^2+myu2.^2);
mymag2=0.5*((myb1-myb1(1,1)).^2+myb2.^2);
%if cntr==1
%mxens2=max(myens(:));
%mxape2=max(myape(:));
%mxke2=max(myke(:));
%mxmag2=max(mymag(:));
%end
figure(1)
subplot(2,3,cntr+3)
contourf(lats,lons,25*myens-myens2,80),shading flat%colorbar
caxis([-1 1]*mxens2*0.1)
axis square
figure(10)
subplot(2,3,cntr+3)
contourf(lats,lons,25*mymag-mymag2,80),shading flat%colorbar
caxis([-1 1]*mxmag2*0.1)
axis square
drawnow
end
figure(1)
subplot(2,3,1)
ylabel('lat')
subplot(2,3,4)
ylabel('lat')
for ni=4:6
subplot(2,3,ni)
xlabel('lon')
end
figure(10)
subplot(2,3,1)
ylabel('lat')
subplot(2,3,4)
ylabel('lat')
for ni=4:6
subplot(2,3,ni)
xlabel('lon')
end
