

% Run in floor-level folder, so add code folder
addpath('new-m-files');

% Time
tmax=30;
t = 0:tmax;

% Reference
DmetricEta_Reference = Compute_Dmetric_Eta('65','1e2','1',tmax);

% Scope G
DmetricEta_HalvedG = Compute_Dmetric_Eta('65','5e1','1',tmax);
DmetricEta_DoubledG = Compute_Dmetric_Eta('65','5e2','1',tmax);
DmetricEta_antiHalvedG = Compute_Dmetric_Eta('65','-5e1','1',tmax);
DmetricEta_antiReference = Compute_Dmetric_Eta('65','-1e2','1',tmax);
DmetricEta_antiDoubledG = Compute_Dmetric_Eta('65','-5e2','1',tmax);

% Scope B
DmetricEta_B00 = Compute_Dmetric_Eta('65','1e2','0',tmax);
DmetricEta_B01 = Compute_Dmetric_Eta('65','1e2','0.1',tmax);
DmetricEta_B05 = Compute_Dmetric_Eta('65','1e2','0.5',tmax);

% Scope lat
DmetricEta_L00 = Compute_Dmetric_Eta('0','1e2','1',tmax);

% Save
save('Dmetrics_Eta.mat');


 % Plots
 subplot(3,1,1)
 plot(t,DmetricEta_Reference,t,DmetricEta_HalvedG,t,DmetricEta_DoubledG,t,DmetricEta_antiReference,t,DmetricEta_antiHalvedG,t,DmetricEta_antiDoubledG);
 title('Scope G')
 xlabel('Time')
 ylabel('Dmetric According to Eta')
 legend('Reference','Halved G','Doubled G','Anti Reference','Anti Halved G','Anti Doubled G')
 
 subplot(3,1,2)
 plot(t,DmetricEta_Reference,t,DmetricEta_B05,t,DmetricEta_B01,t,DmetricEta_B00);
 title('Scope G')
 xlabel('Time')
 ylabel('Dmetric According to Eta')
 legend('Reference','B = 0.5 B0','B = 0.1 B0','B = 0.0 B0')

 subplot(3,1,3)
 plot(t,DmetricEta_Reference,t,DmetricEta_L00)
 title('Scope Latitude')
 xlabel('Time')
 ylabel('Dmetric According to Eta')
 legend('Reference','Lat = 0')
