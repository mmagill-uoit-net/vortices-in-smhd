



function [myvort, myeta, myu1, myu2, myb1, myb2] = full_read(casename,timestep)


    myvort=readsmhddata(sprintf('%s_vort_%05d.dat',casename,timestep),0);
    myeta=readsmhddata(sprintf('%s_e_%05d.dat',casename,timestep),0);
    myu1=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep),0);
    myu2=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep),0);
    myb1=readsmhddata(sprintf('%s_b1_%05d.dat',casename,timestep),0);
    myb2=readsmhddata(sprintf('%s_b2_%05d.dat',casename,timestep),0);
    