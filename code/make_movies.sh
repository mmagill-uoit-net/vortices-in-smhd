#!/bin/bash




## Noisy vorticity doesn't seem encode-able in mkv

#for field in e vort b1 b2 u1 u2
#for field in e vort b1
for field in vort u1 u2
do
    avconv -r 10 -i plots/12_1_65_100_2_1e-6_5e2_1_0_$field.dat_plot%d.png -c:v libx264 -crf 0 movies/SuperVortex_$field.mkv
done
