for ii=6:6
myvort=readsmhddata('./data/10_1_65_100_2_1e-6_1e2_1_0_vort.dat',ii);
myeta=readsmhddata('./data/10_1_65_100_2_1e-6_1e2_1_0_e.dat',ii);
myu1=readsmhddata('./data/10_1_65_100_2_1e-6_1e2_1_0_u1.dat',ii);
myu2=readsmhddata('./data/10_1_65_100_2_1e-6_1e2_1_0_u2.dat',ii);
myb1=readsmhddata('./data/10_1_65_100_2_1e-6_1e2_1_0_b1.dat',ii);
myb2=readsmhddata('./data/10_1_65_100_2_1e-6_1e2_1_0_b2.dat',ii);

myens=0.5*myvort.^2;
myape=0.5*myeta.^2;
myke=0.5*(myu1.^2+myu2.^2);
mymag=0.5*((myb1-myb1(1,1)).^2+myb2.^2);
ensscl=max(myens(:));
apescl=max(myape(:));
kescl=max(myke(:));
magscl=max(mymag(:));
figure(ii)
clf
colormap darkjet
subplot(3,2,1)
imagesc(myens/ensscl),shading interp,colorbar
caxis([0 1])
subplot(3,2,2)
imagesc(myke/kescl),shading interp,colorbar
caxis([0 1])
%caxis([0 1]*0.5*10);
figure(ii+10)
clf
colormap darkjet
subplot(3,2,1)
imagesc(myape/apescl),shading interp,colorbar
caxis([0 1])
%caxis(100+[0 1]*2*10)
subplot(3,2,2)
imagesc(mymag/magscl),shading interp,colorbar
caxis([0 1])
%caxis([0 1]*1*10)
myvort=readsmhddata('./data/10_1_65_100_2_1e-6_5e1_1_0_vort.dat',ii);
myeta=readsmhddata('./data/10_1_65_100_2_1e-6_5e1_1_0_e.dat',ii);
myu1=readsmhddata('./data/10_1_65_100_2_1e-6_5e1_1_0_u1.dat',ii);
myu2=readsmhddata('./data/10_1_65_100_2_1e-6_5e1_1_0_u2.dat',ii);
myb1=readsmhddata('./data/10_1_65_100_2_1e-6_5e1_1_0_b1.dat',ii);
myb2=readsmhddata('./data/10_1_65_100_2_1e-6_5e1_1_0_b2.dat',ii);

myens2=0.5*myvort.^2;
myape2=0.5*myeta.^2;
myke2=0.5*(myu1.^2+myu2.^2);
mymag2=0.5*((myb1-myb1(1,1)).^2+myb2.^2);
ensscl2=max(myens2(:));
apescl2=max(myape2(:));
kescl2=max(myke2(:));
magscl2=max(mymag2(:));
figure(ii)
subplot(3,2,3)
imagesc(myens/ensscl-myens2/ensscl2),shading interp,colorbar
caxis([-1 1]*0.1)
subplot(3,2,4)
imagesc(myke/kescl-myke2/kescl2),shading interp,colorbar
caxis([-1 1]*0.1)
subplot(3,2,5)
imagesc(myens2/ensscl2),shading interp,colorbar
caxis([0 1])
subplot(3,2,6)
imagesc(myke2/kescl2),shading interp,colorbar
caxis([0 1])
figure(ii+10)
subplot(3,2,3)
imagesc(myape/apescl-myape2/apescl2),shading interp,colorbar
caxis([-1 1]*0.1)
subplot(3,2,4)
imagesc(mymag/magscl-mymag2/magscl2),shading interp,colorbar
caxis([-1 1]*0.1)
subplot(3,2,5)
imagesc(myape2/apescl2),shading interp,colorbar
caxis([0 1])
%caxis(100+[0 1]*2*10)
subplot(3,2,6)
imagesc(mymag2/magscl2),shading interp,colorbar

drawnow
end
