#ifndef _MESHES_H_
#define _MESHES_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <omp.h>
#include "constants.h"
#include "variables.h"

struct Meshes {

   // Store Mesh Sizes
   int Nx; int Ny;
   double dx; double dy;
   double dk; double dl;

   // Physical dimensions
   double Lx; double Ly;  // x goes from -Lx to Lx
   double latitude;       // Latitude at which mesh is centered
   double H;              // Depth of the fluid layer

   // Physical input parameters
   double omega;          // Rate of rotation
   double R;              // Radius
   double gR;             // Reduced gravity (incorporating buoyancy)
   double B0;             // Background magnetic field (in velocity units)
   double G;              // Initial circulation

   // Physical resultant parameters
   double f;              // Rotation factor for f-plane approximation
   int BetaFlag;          // Whether to use beta plane or f-plane
   double c0;             // Gravity wave speed

   // Variables for spatial meshes
   double* x; double* y;

   // Variables for Fourier meshes + filters
   double* k; double* l;
   double* k_filter; double* l_filter;

   // Variables for fluid properties
   //    i is previous timestep
   //    j is current timestep
   //    k is next timestep 
   struct Variables* e_i;
   struct Variables* e_j;
   struct Variables* e_k;

   struct Variables* u1_i;
   struct Variables* u1_j;
   struct Variables* u1_k;
   struct Variables* u2_i;
   struct Variables* u2_j;
   struct Variables* u2_k;

   struct Variables* b1_i;
   struct Variables* b1_j;
   struct Variables* b1_k;
   struct Variables* b2_i;
   struct Variables* b2_j;
   struct Variables* b2_k;

   // FFTW Variables for the nonlinear terms in height evolution
   struct Variables* NLT1_i;
   struct Variables* NLT1_j;
   struct Variables* NLT1_k;
   struct Variables* NLT2_i;
   struct Variables* NLT2_j;
   struct Variables* NLT2_k;

   // Output files
   char* casename;
   int timestep;
   char fe[512];
   char fu1[512];
   char fu2[512];
   char fb1[512];
   char fb2[512];
   char fvort[512];

   // Plotting
   int plotting;
   FILE* gnuplot_pipe;

};

#include "ICs.h"

//// //// //// //// //// ////


void InitializeMesh(struct Meshes* , int, int, double, double, double, double, double, double, double, double, double, int, int, char*);
void FreeMesh(struct Meshes*);


//// //// //// //// //// ////


void FillTi(struct Meshes*);  // Fill time i with ICs


//// //// //// //// //// ////


void EulerStep(struct Meshes*, double);  // Fill time j with time i
int LeapFrog(struct Meshes*, double);   // Fill time k with times i and j
void PrintMeshToFile(struct Meshes*, int);      // Print each variable field at timestep j


//// //// //// //// //// ////

#endif
