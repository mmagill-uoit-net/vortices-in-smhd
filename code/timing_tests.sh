#!/bin/bash

# Output from time will be real time elapsed in seconds to 3 decimal places
TIMEFORMAT='%3R'

# Set number of threads
export OMP_NUM_THREADS=8

# Function running the cases 5 times each
run5tests() {
for i in {1..5}; do
echo -n "$1 " >> times.dat
(time ./bin/spectralsmhd.exe $1 $2 $3 $4 $5 $6 $7 ) 1>>/dev/null 2>> times.dat
done
}

# Remove any previous times.dat
rm -f times.dat

# Constants for all tests
NxByNy=2
Layer=2
alpha=1e-5
plotflag=0


#                             log2(Ny)  (Nx/Ny)  latitude  Lx(degs)  Rad,Ovr  alpha   plotting_flag
echo "Doing Case 1"
echo "Case 1" >> times.dat
for size in {3..9}; do
echo -e "\tSize: $size"
run5tests                     $size    $NxByNy      5        1      $Layer   $alpha   $plotflag
done 

echo "Doing Case 2"
echo "Case 2" >> times.dat
for size in {3..9}; do
echo -e "\tSize: $size"
run5tests                     $size    $NxByNy      5       100     $Layer   $alpha   $plotflag
done 

echo "Doing Case 3"
echo "Case 3" >> times.dat
for size in {3..9}; do
echo -e "\tSize: $size"
run5tests                     $size    $NxByNy     65       100     $Layer   $alpha   $plotflag
done 


