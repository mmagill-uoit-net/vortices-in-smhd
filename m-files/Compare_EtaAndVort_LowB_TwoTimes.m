Ncontours=40;
G='1e2';
Bfac='0.1';
casename=sprintf('../data/10_1_65_100_2_1e-6_%s_%s_0',G,Bfac);
timestep=60;
timestep2=600;
fig21=figure(221)
clf
betterplots
colormap darkjet
myeta=readsmhddata(sprintf('%s_e_%05d.dat',casename,timestep),0);
myeta2=readsmhddata(sprintf('%s_e_%05d.dat',casename,timestep2),0);

myu1=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep),0);
myu2=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep),0);
    % Wave vectors
    N=1024;
    L=(100./360.) * 2*pi*0.7*6.955e8;
    k=[0:N/2-1 0 -N/2+1:-1]';
    dk = pi / L; k = dk*k; l=k;
    [kk,ll]=meshgrid(k,l);
    
    % Vorticity
    myu1y = real(ifft2( i* ll.*fft2(myu1) ));
    myu2x = real(ifft2( i* kk.*fft2(myu2) ));
    myvort = myu2x - myu1y;
myu12=readsmhddata(sprintf('%s_u1_%05d.dat',casename,timestep2),0);
myu22=readsmhddata(sprintf('%s_u2_%05d.dat',casename,timestep2),0);
    % Wave vectors
    N=1024;
    L=(100./360.) * 2*pi*0.7*6.955e8;
    k=[0:N/2-1 0 -N/2+1:-1]';
    dk = pi / L; k = dk*k; l=k;
    [kk,ll]=meshgrid(k,l);
    
    % Vorticity
    myu1y2 = real(ifft2( i* ll.*fft2(myu12) ));
    myu2x2 = real(ifft2( i* kk.*fft2(myu22) ));
    myvort2 = myu2x2 - myu1y2;

subplot(2,2,1)
%imagesc(myape/mxape),shading interp
deta=(myeta-myeta2*5)/max(abs(myeta(:)));
%contourf(myeta/max(myeta(:)),Ncontours),shading flat
imagesc(myeta/max(myeta(:))),shading flat
caxis([-1 1])
%caxis([0,1]);
axis equal
axis off
text(100,100,'(a)','Color','w')
xlabel('lon')
subplot(2,2,2)
%imagesc(mymag/mxmag),shading interp
dvort=(myvort-myvort2)/max(abs(myvort(:)));
%contourf(myvort/max(myvort(:)),Ncontours),shading flat
imagesc(myvort/max(myvort(:))),shading flat
caxis([-1 1])
axis equal
axis off
text(100,100,'(b)','Color','w')
ylabel('lat')
xlabel('lon')

subplot(2,2,3)
%imagesc(myape/mxape),shading interp
deta=(myeta-myeta2*5)/max(abs(myeta(:)));
%contourf(myeta2/max(myeta2(:)),Ncontours),shading flat
imagesc(myeta2/max(myeta2(:))),shading flat
caxis([-1 1])
%caxis([0,1]);
axis equal
axis off
text(100,100,'(c)','Color','w')
xlabel('lon')
subplot(2,2,4)
%imagesc(mymag/mxmag),shading interp
dvort=(myvort-myvort2)/max(abs(myvort(:)));
%contourf(myvort2/max(myvort2(:)),Ncontours),shading flat
imagesc(myvort2/max(myvort2(:))),shading flat
caxis([-1 1])
axis equal
axis off
text(100,100,'(d)','Color','w')
ylabel('lat')
xlabel('lon')

etaratio=max(abs(myeta(:)))/(5*max(abs(myeta2(:))))
vortratio=max(abs(myvort(:)))/(5*max(abs(myvort2(:))))
tightfig(fig21)
