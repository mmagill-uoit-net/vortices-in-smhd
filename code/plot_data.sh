#!/bin/bash

logNy=$1
AspectRatio=$2
latitude=$3
width=$4
layer=$5
alpha=$6
circ=$7
magflag=$8
plotflag=$9
field=${10}
range=${11}

nrows=$((2**$logNy))
name="$logNy"_"$AspectRatio"_"$latitude"_"$width"_"$layer"_"$alpha"_"$circ"_"$magflag"_"$plotflag"_"$field"


nT=90

start=1
for i in $(eval echo {1..$nT})
do

   echo "Plotting time $i..."

   

   sed -n "$start,$(($start+$nrows-1)) p" < data/$name.dat > plots/$name"_"$i.dat
   gnuplot -p << EOF
set terminal postscript eps enhanced color
set output 'plots/${name}_${i}.eps'
set pm3d map
set cbrange [-${range}:${range}]
splot 'plots/${name}_${i}.dat' matrix with image
EOF

   convert -density 300 plots/${name}_${i}.eps -transparent white plots/${name}_${i}.png

   start=$(($start+$nrows))

done

echo "Done plotting, making movie..."
avconv -r 10 -i plots/"$name"_%d.png movies/$name.mp4
