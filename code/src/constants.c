#include "constants.h"


//// Math constants ////

double pi = 3.14159;


//// Physical Constants ////


// Solar Parameters (from Wikipedia)
double Radius_Sun = 6.955e8;   // Radius in m
double g_Sun = 274;         // Surface gravity in m/s2

// Solar Rotation in deg/day
double omegaA_Sun = 14.713; 
double omegaB_Sun = -2.396; 
double omegaC_Sun = -1.787;
double omega_Sun (double lat) {
   return omegaA_Sun + omegaB_Sun*pow(sin(lat),2) + omegaC_Sun*pow(sin(lat),4);
}


// Tachocline - general
double Radius_Tach() {                         // Radius of the tachocline
   return 0.7*Radius_Sun;
}
double g_Tach() {           // Gravity in the tachocline
   return g_Sun / pow(Radius_Tach()/Radius_Sun,2);
}
double circumf_Tach() {             // Circumference of the tachocline
   return 2*pi*Radius_Tach();
}
double rho_Tach = 210;                          // Tachocline density
double realB_Tach = 10;                         // From Schecter (originally in Gauss, here in Tesla)
double B_Tach() {                               // Scale magnetic field into units of velocity
   return realB_Tach / sqrt(4*pi*rho_Tach*1e-7);
}
// Tachocline - rotation
double omega_Tach() {                            // Tachocline rotation in rad/sec
   double deg_per_day = omega_Sun(pi/4);
   double rad_per_day = deg_per_day * pi/180;
   double rad_per_sec = rad_per_day / (24*60*60);
   return rad_per_sec;
}
// Tachocline - radiative layer
double H_Tach_Rad = 1e7;                                        // From Schecter (originally in cm, here in m)
double gAlpha_Tach_Rad = 0.1;                                   // From Schecter (alpha = 0.01 - 0.3)
double gR_Tach_Rad() {                   // Effective gravity in radiative layer
   return g_Tach() * gAlpha_Tach_Rad;
}
// Tachocline - overshoot layer
double H_Tach_Ovr = 1e7 / 2;                             // Overshoot layer is half as thick
double gAlpha_Tach_Ovr = 1e-5;                               // From Schecter (alpha = 1e-6 - 1e-4)
double gR_Tach_Ovr() {                   // Effective gravity in overshoot layer
   return g_Tach() * gAlpha_Tach_Ovr;
}

//// End of Physical Constants ////


