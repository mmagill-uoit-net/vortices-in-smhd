

function analyse_energies(timestep,G,Bfac)

    % Load
    casename=sprintf('./data/10_1_65_100_2_1e-6_%s_%s_0',G,Bfac);
    [myvort, myeta, myu1, myu2, myb1, myb2] = full_read(casename,timestep);

    % Compute fields
    [myens, myape, myke, mymag] = compute_energies(myvort,myeta,myu1,myu2,myb1,myb2);

    % Plot
    figure('Name',sprintf('Timestep %d, G = %s, B = %s B_0',timestep,G,Bfac),'NumberTitle','off')
    plot_energies(myens,myke,myape,mymag);



